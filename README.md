# peertube-autoRedundant
Activate the redundancy for all instances automaticaly.

## Usage
```sh
./updateANode
```
Then your instances will be updated (activate redundancy for all instances).

# Update a node
This script can't be lunched by cron the first time (cause it will ask you question for create a json config file, secret.json).
```sh
./updateANode.py
```
If you would do secret.json file manualy here is a template (for be sure to don't be have conflict with web interface you should create a new administrator account named script and only used by script) :
```json
{
    "node":"URL OF YOUR NODE WITHOUT HTTPS://",
    "account":"ACCOUNT NAME WITH ADMIN RIGHT",
    "password":"PASSWORD OF THIS ACCOUNT"
}
```
Example :
```json
{
    "node":"peertube.jorropo.ovh",
    "account":"script",
    "password":"123456"
}
```
