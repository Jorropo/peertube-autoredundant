#!/usr/bin/env python3
#-----------------------------------------import
import json
import sys
import os
from urllib.parse import urlencode
from urllib.request import Request, urlopen

#-----------------------------------------global declaration
global key
key = []

def activate(instance):
    urlopen(Request("https://"+key["node"]+"/api/v1/server/redundancy/"+instance,'{"redundancyAllowed":true}'.encode("ascii"), method="PUT",headers={"Authorization":"Bearer "+key["token"]["access_token"],"Content-Type":"application/json"}),timeout=15).read().decode()

#-----------------------------------------key conf loading
os.chdir(os.path.dirname(os.path.realpath(__file__)))
if not os.path.exists("secret.json"):
    print("you don't have any key configuration, let's do one right know")#use print for crash with broken pipe if there isn't any conf
    st = '{\n    "node":"'+input("node name (without https://) ? ")+'",\n    "account":"'+input("account name (with admin right) ? ")+'",\n    "password":"'+input("accout password ? ")+'"\n}\n'#use a temp string for one step make memory foot print bigger but if user cancel this no empty file will be created
    f = open("secret.json","w")
    f.write(st)
    f.close()
f = open("secret.json","r")
key = json.loads(f.read())
f.close()

r = json.loads(urlopen(Request("https://"+key["node"]+"/api/v1/oauth-clients/local"),timeout=15).read().decode())
key["token"] = json.loads(urlopen(Request("https://"+key["node"]+"/api/v1/users/token",urlencode({"client_id":r["client_id"],"client_secret":r["client_secret"],"grant_type":"password","response_type":"code","username":key["account"],"password":key["password"]}).encode()),timeout=15).read().decode())

#-----------------------------------------updating
nc = json.loads(urlopen(Request("https://"+key["node"]+"/api/v1/server/following?count=0"), timeout=15).read().decode())["total"]
nt = []
for i in range(0,nc,100):
    nt += json.loads(urlopen(Request("https://"+key["node"]+"/api/v1/server/following?count=100&start="+str(i)), timeout=15).read().decode())["data"]
for i in nt:
    if not i["following"]["hostRedundancyAllowed"]:
        print("Activing for " + i["following"]["host"])
        activate(i["following"]["host"])
    else:
        print("all ready activated for " + i["following"]["host"])

#-----------------------------------------finish
sys.exit(0)
